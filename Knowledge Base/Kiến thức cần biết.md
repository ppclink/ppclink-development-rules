#1. [Hiểu về lập trình hướng đối tượng (OOP)](https://daynhauhoc.com/t/the-nao-la-lap-trinh-huong-doi-tuong-oop/1519)
#2. [Các kiến trúc lập trình (MVC, MVP, MVVM, VIPER...)](https://medium.com/swlh/ios-design-patterns-a9bd07818129)
## Tại sao nên chọn MVC cho lập trình iOS tại công ty
    * MVC là pattern được Apple suggest cho việc viết ứng dụng iOS, bản thân các framework cocoa, uikit… cũng được viết theo MVC.
    * MVC cho tốc độ viết ứng dụng nhanh, dễ tiếp cận và dễ maintain, dễ quản lý references count (tránh memmory leak).
    * Nhược điểm lớn nhất của MVC là dễ gây tình trạng Massage View Controller (1 viewcontroller sẽ bị phình quá to khi viewcontroller đó có nhiều tính năng, logic...). Tuy nhiên nếu nắm rõ OOP và iOS thì chúng ta có thể giải quyết đơn giản:
     - Thực hiện chia nhỏ các tính năng của Viewcontroller cha thành các viewcontroller con (Mỗi viewcontroller con thực hiện một chức năng cố cụ thể, có thể reuse ở nhiều chỗ)
     - Dùng Container view hoặc addsubview... để merger các viewcontroller nhỏ vào với nhau
     - Tối đa hoá reuse code (reuse cả UI và code logic) 
     => Có thể tham khảo code viettel post có những màn như tạo đơn rất dài nhưng chia nhỏ ra thành rất nhiều viewcontroller con, mỗi viewcontroller con code không quá 500 dòng.
#3. [RESTfull API, JSON](https://viblo.asia/p/thiet-ke-restful-api-GrLZD98Vlk0)
#4. [SQL database và NoSQL database](https://www.newgenapps.com/blog/sql-vs-nosql-finding-the-right-dbms-for-your-project)
#5. Firebase, realm