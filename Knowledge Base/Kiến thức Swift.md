## 1. Hiểu rõ về Optionals, khi nào dùng optionals và các cách unwrapper optionals
* [Link](https://medium.com/ios-os-x-development/optionals-in-swift-for-newbies-7199a30707d5)
## 2. Phân biệt [value type và reference type](https://viblo.asia/p/so-sanh-reference-vs-value-types-trong-swift-p1-Eb85oEN2Z2G), khác nhau giữa Struct và Class. Khi nào sử dụng Struct, khi nào sử dụng class
## 3. Hiểu rõ [Closure](https://viblo.asia/p/gioi-thieu-ve-closure-trong-swift-ios-1Je5E8z0lnL)
* Closure trong swift cực kì mạnh, dùng để callback, pass data, handler event... nên cần hiểu rõ closure.
* [Tránh memory leak trong closure](https://phantuanvi.com/2017/03/tìm-hiểu-rò-rỉ-bộ-nhớ-trong-closure/)
## 4. Ứng dụng [extension](https://fabiti.com/apple/swift/bai-18-ky-thuat-mo-rong-class-bang-extension.html)
## 5. ARC, các cách tránh cycle reference count dẫn tới memory leak
* [Link](https://www.raywenderlich.com/959-arc-and-memory-management-in-swift)
## 6. Multi threading, GCD
* [Link](https://www.raywenderlich.com/740-grand-central-dispatch-tutorial-for-swift-3-part-1-2)