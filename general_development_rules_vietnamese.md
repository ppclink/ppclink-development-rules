# PPCLINK Development rules
***Một số quy tắc chung trong quá trình phát triển phần mềm, yêu cầu dev cần tư duy hiểu ý nghĩa và áp dụng một cách linh hoạt.***

**Không viết lặp/thừa code**

- Nếu có 1 đoạn code mà dùng ở nhiều chỗ giống nhau hoặc chỉ khác nhau tham số đầu vào, đừng copy/paste cả đoạn code mà hãy tạo 1 hàm với tham số để thực hiện những công việc đó và gọi hàm ở nhiều chỗ.

**Đảm bảo code có tính linh hoạt & mở rộng trong tương lại**

- Trước khi bắt tay vào viết code, dev cần tư duy & trao đổi với techlead để thiết kế cấu trúc code sao cho có thể đáp ứng được những thay đổi & mở rộng tính năng trong tương lai.

**Đơn giản là tốt nhât:** 

- Cố gắng tư duy/cải tiến để logic code đơn giản & dễ hiểu nhất có thể. Điều này có thể mất thời gian ban đầu nhưng sẽ giúp cho việc đọc lại code và debug lỗi sau này dễ dàng hơn.

**Hạn chế comment trong code**

- Khi viết code (đặt tên hàm biến,..) một cách rõ ràng & theo quy ước chung thì không cần phải comment quá nhiều để có thể hiểu.

**Đặt tên**

- Phải rõ nghĩa mô tả đúng chức năng/vai trò của đối tượng được đặt tên. VD: không nên đặt user1, user2 mà nên đặt dạng purchasedUser, checkoutUser...
- Không nên viết tắt. Ví dụ: ko viết "Cat" thay cho "category"

**Tìm kiếm giải pháp cho một vấn đề**

- Không nên nhờ sự trợ giúp của người khác ngay khi bạn chưa suy nghĩ tư duy để hiểu vấn đề, để biết mình đang cần gì, nên thử một số giải pháp trên Internet trước. Nếu không được mới nên hỏi để hạn chế làm phiền người khác.
- Hãy luôn cố gắng tìm kiếm giải pháp tốt nhất cho vấn đề: 
- Đừng lúc nào cũng chọn giải pháp đầu tiên tìm được hoặc giải pháp dễ làm nhất mà hãy tư duy tìm kiếm thêm để có khoảng 2-3 giải pháp cho vấn đề. Sau đó hãy cân nhắc để lựa chọn giải pháp nào tốt & phù hợp nhất cho vấn đề ở thời điểm đó. Hãy luôn có tinh thần sáng tạo & mong muốn tìm được cách giải quyết vấn đề tối ưu hơn.

**Sử dụng thư viện bên thứ 3**

- Nên để ý đến bản quyền & các điều khoản/giấy phép sử dụng của thư viện để có thể sử dụng phù hợp và đúng mục đích. Ưu tiên các thư viện có giấy phép MIT, Apache hơn là GPL/LGPL.


- Nên sử dụng thư viện đã được đóng gói và đăng ký trên các Repo phổ biển để tiện cho việc cập nhật & đảm bảo cả team dùng chung 1 phiên bản của thư viện. Tùy theo ngôn ngữ lập trình sẽ có các Package Manager tương ứng:

| Language          | PackageManager     |
| ----------------- | ------------------ |
| PHP               | Composer           |
| Python            | pip                |
| Swift/Objective-C | CocoaPods/carthage |
| Java              | Gradle             |

- Nên sử dụng bản ổn định (stable) của thư viện, (không dùng phiên bản alpha hoặc development).
- Nên để ý cập nhật thư viện thường xuyên để fix các lỗi chương trình do thư viện gây ra.

**Quy tắc code**

- Tuân theo tài liệu chuẩn code chung của công ty cũng như đặc thù riêng từng dựng án.

