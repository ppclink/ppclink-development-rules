# Quản lí source code
* Tất cả source code đều phải tạo và update trên git của công ty.
* Hàng ngày commit tiến độ công việc.

# Tích hợp thư viện, các thư viện hay dùng
* Sử dụng cocoa pod để tích hợp các thư viện cần dùng, không lạm dụng thư viện chỉ để phục vụ các tác vụ nhỏ. Nếu có các thư viện cocoa pod không hỗ trợ thì phải đặt vào trong folder lib của project.
* Tất cả các project của công ti cần tích hợp Fabric để publish các bản test, Fabric crashlytics để quản lí crash với quyền tài khoản fabric của công ti.
* Tuỳ vào dự án, tích hợp [thư viện quảng cáo chéo và thư viện quảng cáo của công ti](https://trello.com/b/JavHNkdl/ppclink-developers)

# Cấu trúc project