# Android Development Guidelines

## IDE

Use Android Studio.

## Support Android Versions

Default support version is Version 4.0 and later.

## SDK

Use latest SDK.

## Architecture

Use MVP Pattern

## Third party Library

Use gradle to install. And never use non-version controlled libraries.


## Default libraries
Fabric
Crashlytics
PPCLINK-VDFramework-iOS

## Follow development checkist
