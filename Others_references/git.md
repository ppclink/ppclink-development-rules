# Git Repository Working Rules


## You MUST commit/push code everyday to backup on git server

## You MUST Write descriptive message on git commit.

Never write meaningless message like "More fix" or "Bug Fix". You need to write "What did you do" clearly.
Examples:

* Added CLI Wrapper to native Evaluation Mode
* Remove some unnecessary items from gitignore
* Less globals pollution; adding debug-break option
