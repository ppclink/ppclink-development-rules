# Using Third Party Libraries

## Package Manager

You should use package managers.

| Language | PackageManager    |
|:---------|:------------------|
| PHP      | Composer          |
| Python   | pip               |
| Swift/Objective-C    | CocoaPods/carthage|
| Java     | Gradle            |

You should not use  third party library which is not registered to package manager registry.

It's for ensuring every developers can use same version of library.

## Version Management

You must specify at least minor version "x.x" when you use third party libraries. Should not specify only "major" version.
Should not use "development" version or "alpha" version.

Because if you don't fix the version, app may stop working when the library introduces braking changes.

## Old Libraries

You should not use too old libraries. If the library has not updated more than 1 year. You should try to find newer libraries.

## License

You need to check third party library license especially you need to bundle it to the app binary ( using in client applications ). You can only use the libraries distributed with the following licenses.

* MIT License
* Apache License

Never use GPL/LGPL and other licenses.

## Isn't it too huge ?

The library's core function is the function which you need to use in the app? If you want to use library for using it's "sub" feature, you may not use it and try to find alternatives ( or implement by your self )
