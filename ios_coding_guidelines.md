iOS Good Practices
==================

## Mục lục

1. [Getting Started](#header-1)
1. [Những thư viện phổ biến và khuyên dùng](#header-2)
1. [Architecture](#header-3)
1. [Stores](#header-4)
1. [Coding Formatting](#header-5)
1. [Cách đặt tên biến, hàm, class...](#header-6)
1. [Coding Style](#header-7)
1. [Documentation/Comments](#header-8)
1. [Diagnostics](#header-9)

## Getting Started 

### Human Interface Guidelines

Để lập trình được một ứng dụng thân thiện, có tính liền lạc và thống nhất với hệ điều hành iOS, trước hết chúng ta cần hiểu về những gì hệ điều hành iOS có thể làm, và các chuẩn mà Apple đề xuất. Đọc tại Apple's [Human Interface Guidelines][ios-hig].

[ios-hig]: https://developer.apple.com/ios/human-interface-guidelines/

### Project Setup
Hiện tại để xây dựng giao diện cho ứng dụng iOS có các hình thức: Khởi tạo và layout bằng code, dùng file Xib và dùng Storyboard. 

#### Khởi tạo và layout bằng code
* Phương pháp này phổ biến khi Xib và Storyboard chưa được giới thiệu, hiện tại không được sử dụng nhiều do code dài, dev và người mantain sau khó hình dung được giao diện.
#### Dùng file Xib
* File Xib sẽ sử lí init layout và giao diện theo phương pháp kéo thả, trực quan. Thuận tiện cho việc sử dụng lại, giữ codebase [DRY][dry]
[dry]: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself
#### Dùng Storyboard
* Storyboard là phương thức mới nhất được Apple giới thiệu, nó giúp dev và maintainer có cái nhìn tổng quan về luồng của app, các mối liên hệ giữa các màn hình... Giúp code nhanh hơn và trực quan hơn với những app nhỏ, với những app lớn khi Storyboard chứa quá nhiều Board sẽ nặng và cồng kềnh.

=> Chúng ta chọn dùng file Xib để thiết kế giao diện, Khởi tạo và layout bằng code áp dụng với những Common Component đơn giản (như custom Button, custom Label...)

### Git và Git Ignore
Để phù hợp với làm việc nhóm, đảm bảo an toàn cho source code và thuận tiện khi thay đổi thiết bị làm việc... Tất cả các project phải được tạo trên Git, commit và push code mỗi cuối ngày theo [Git flow][git-flow].
Khi commit và push code lên Git, chúng ta chỉ push những file dùng chung, những file cài đặt cá nhân, file tạm... sẽ không cần push lên git. Tạo cài đặt các file ignore mặc định tại [GitIgnore][swift-gitignore]
[swift-gitignore]: https://github.com/github/gitignore/blob/master/Swift.gitignore
[git-flow]: https://backlog.com/git-tutorial/vn/stepup/stepup1_5.html

### Quản lý thư viện, framework
Các thư viện, framework đơn giản cần custom riêng chúng ta để vào thư mục Library trong Bundle. Các thư viện và framework khác sẽ dùng [CocoaPods][cocoapods] để quán lý.

[cocoapods]: https://cocoapods.org/

### Cấu trúc Project

Để dễ tìm kiếm và thuận tiện trong quá trình code, review... Các file sẽ được đặt ở các thư mục thể hiện tính năng, thuộc tính của các file đó. Cấu trúc thư mục mẫu của project như sau:

	├─ Models
	  ├─ User.swift // user model
	  ├─ Post.swift  // post model
	├─ Resources
	  ├─ Images.xcassets
	  ├─ Font
	  ├─ Sounds
	├─ Application
	  ├─ LaunchScreen.storyboard
	  ├─ AppDelegate.swift
	├─ ViewControllers
	  ├─ Login
		├─ LoginViewController.swift // view controller
		├─ LoginViewController.xib
	  ├─ Feed
		├─ FeedViewController.swift
		├─ FeedViewController.xib
		├─ FeedTableViewCell.swift 
	├─ Helpers
	  ├─ Constants.swift // Tổng hợp các constant dùng trong global trong app, những constant nào chỉ dùng trong 1 class thì nên đặt ở class đó.
	  ├─ Common
		├─ Controls
		  ├─ BouncingButton.swift // Component dùng chung trong cả app (ví dụ: label, button, textfield... để có khi sửa thì chỉ cần sửa ở một chỗ và app có tính đồng bộ cao)
	  ├─ Extensions
		├─ UIView+Additions.swift // useful additions to UIView
		├─ NSURL+Additions.swift // useful additions to NSURL


### Xác định phiên bản iOS hỗ trợ thấp nhất

Do có sự khác nhau khá lớn về API giữa các bản iOS nên rất cần thiết khi xác định nên hỗ trợ từ phiên bản nào để đạt được hiệu quả cao nhất. Một trong các yếu tố để xác định hỗ trợ từ phiên bản iOS nào đó là tỉ lệ người dùng các phiên bản iOS, sử dụng một số nguồn sau để tham khảo:

* Thông tin chính thống từ Apple:
    * [Apple’s world-wide iOS version penetration statistics](https://developer.apple.com/support/app-store/).
* Dữ liệu từ bên thứ ba:
    * [iOS Support Matrix](http://iossupportmatrix.com).
    * [Mixpanel Trends: iOS versions](https://mixpanel.com/trends/#report/ios_frag).

## Những thư viện phổ biến và khuyên dùng 

Việc lựa chọn sử dụng các thư viện nên dựa theo các tiêu chí: Thư viện được cộng đồng tin dùng và đánh giá cao (số star trên GitHub), thư viện được maintain đầy đủ (tránh thư viện đã ngừng maintain từ lâu)...

### AFNetworking/Alamofire

Thư viện hỗ trợ kết các tác vụ liên quan đến RESTful API, download, upload... được viết dựa vào NSURLSession: [Alamofire][alamofire-github] từ khi được giới thiệu tới nay vẫn chưa tìm được đối thủ.
[alamofire-github]: https://github.com/Alamofire/Alamofire

### SwiftDate

Các tác vụ về ngày tháng, tính toán ngày tháng thường khá dài và dễ gây lỗi. Vậy nên tự viết một thư viện về ngày tháng là không được khuyến khích [don't write your date calculations yourself][timezones-youtube]. Thư viện vô địch về tính toán, convert ngày tháng hiện tại là [SwiftDate][swift-date]
[timezones-youtube]: https://www.youtube.com/watch?v=-5wpm-gesOY
[swift-date]: https://github.com/malcommac/SwiftDate

### Auto Layout

Đôi khi những custom component hoặc view nhỏ chúng ta không muốn sử dụng file Xib, để autolayout nên dùng [SnapKit][snapkit]

[snapkit-github]: https://github.com/SnapKit/SnapKit

## Architecture

* [Model-View-Controller-Store (MVCS)][mvcs]
    * Đây là mô hình được được Apple dùng để viết framework Cocoa, và cũng là mô hình Apple khuyến khích sử dụng (MVC), được mở rộng thêm phần Store để quản lý các tác vụ giữa Model và server API, cache... cho rạch ròi. MVC nếu không xử lý bằng cách chia nhỏ View to thành các View nhỏ (dùng container view), sẽ rất dễ đến hiện tượng 'massive view controllers': hiện tượng class viewcontroller quá lớn khi lượng tính năng trên một view lớn (có thể lên tới hàng nghìn dòng...)
* [Model-View-ViewModel (MVVM)][mvvm]
    * Đây là mô hình xu hướng đang được sử dụng trên nhiều nền tảng lập trình, sẽ giải quyết được vấn đề "massive view controllers" bằng cách chia nhỏ ViewController thành ModelView và View. Phần ModelView có thể reuse, dễ dàng viết kiểm thử..., rất hữu ích với các dự án lớn. Tuy nhiên MVVM chưa thực sự phù hợp với iOS do cần cơ chế binding data linh hoạt. Nếu sử dụng MVVM có thể kết hợp với RxSwift, RxCocoa... để binding data dễ dàng hơn.
    * Tìm hiểu thêm MVVM qua bài viết của Bob Spryn: [fantastic introduction][sprynthesis-mvvm].
* Các mô hình khác
	* Ngoài MVC và MVVM còn nhiều mô hình khác như VIPER, FLUX, REDUX... Mỗi mô hình sẽ giải quyết các vấn đề cụ thể mà mỗi dự án gặp phải.
Để có sự thống nhất giữa code và các API của framework Cocoa, để dễ tiếp cận và phù hợp với các dự án vừa và nhỏ. MVC vẫn là mô hình được khuyến khích sử dụng. Vấn đề "Massive View Controller" có thể dễ dàng giải quyết khi split View hợp lí.
[mvcs]: http://programmers.stackexchange.com/questions/184396/mvcs-model-view-controller-store
[mvvm]: https://www.objc.io/issues/13-architecture/mvvm/
[sprynthesis-mvvm]: http://www.sprynthesis.com/2014/12/06/reactivecocoa-mvvm-introduction/


### “Event” Patterns

Swift hỗ trợ nhiều phương thức để chuyển dữ liệu, callback... giữa các class:

* __Delegation:__ _(one-to-one)_ Được sử dụng rất nhiều trong framework Cocoa.
* __Callback blocks:__ _(one-to-one)_ Linh hoạt và nhanh gọn, đang được sử dụng để thay thế cho delegate và những việc delegate không làm được.
* __Notification Center:__ _(one-to-many)_ Khi sử dụng Notification Center cần quản lý observer chặt, hạn chế sử dụng.
* __Key-Value Observing (KVO):__ _(one-to-many)_ Không khuyến khích sử dụng do thiết lập lâu, dễ gây lỗi.

### Models

Nên sử dụng Struct (value type) thay vì Class (reference type) để tạo Model để tránh tình trạng memory leak. Tạo thể hiện của Model từ JSON (lấy từ API về) có thể dùng Codeable trong swift hoặc [SwiftyJson][swiftyjson]

[swiftyjson]: https://github.com/SwiftyJSON/SwiftyJSON

### Views

Bắt buộc sử dụng AutoLayout và Size classes khi dựng layout cho View. Các component được sử dụng lại nhiều trong app nên tạo một custom component và thực hiện subclass khi sử dụng.

## Stores

Khi kết nối API hoặc sử dụng các tác vụ logic nặng, sử dụng dispatch asyn để ứng dụng không bị đơ giao diện. Khi nào cần update UI thì trở về mainthread để thực hiện (mặc định Alamofire tạo các thread và chạy ở thread riêng).

## Coding Formatting

* **1** Cài đặt `tab` bằng khoảng 4 lần `space`.
* **2** Không nên code quá dài trên một dòng. Một dòng tối đa nên nhỏ hơn 160 kí tự (Xcode->Preferences->Text Editing->Page guide at column: 160)
* **3** Cuối mỗi file luôn là một dòng mới.
* **4** Cuối mỗi dòng, file... không có các dấu "space" thừa (Xcode->Preferences->Text Editing->Automatically trim trailing whitespace + Including whitespace-only lines).
* **5** Không mở đầu một khối code bằng cách xuống dòng, chúng ta sử dụng style [1TBS](https://en.m.wikipedia.org/wiki/Indentation_style#1TBS).

```swift
class SomeClass {
    func someMethod() {
        if x == y {
            /* ... */
        } else if x == z {
            /* ... */
        } else {
            /* ... */
        }
    }

    /* ... */
}
```

* **6** Khi định nghĩa một biến, hằng..., dấu `:` luôn theo sát tên biến, hằng... và theo sau dấu `:` là dấu `space`.

```swift
// ví dụ khai báo kiểu dữ liệu
let pirateViewController: PirateViewController

// vị trí dấu `:`
let ninjaDictionary: [String: AnyObject] = [
    "fightLikeDairyFarmer": false,
    "disgusting": true
]

// khai báo một hàm
func myFunction<T, U: SomeProtocol>(firstArgument: U, secondArgument: T) where T.RelatedType == U {
    /* ... */
}

// gọi một hàm và truyền tham số
someFunction(someArgument: "Kitten")

// superclasses
class PirateViewController: UIViewController {
    /* ... */
}

// protocols
extension PirateViewController: UITableViewDataSource {
    /* ... */
}
```

* **7** Bình thường, sau dấu `,` là một dấu `space`.

```swift
let myArray = [1, 2, 3, 4, 5]
```

* **8** Trước và sau các phép toán và logic như `+`, `==`, hoặc `->` là dấu `space`. Không đặt `space` sau khi mở ngoặc `(` và trước khi đóng ngoặc `)`.

```swift
let myValue = 20 + (30 / 2) * 3
if 1 + 1 == 3 {
    fatalError("The universe is broken.")
}
func pancake(with syrup: Syrup) -> Pancake {
    /* ... */
}
```

* **9** Thụt đầu dòng theo gợi ý của Xcode. (Tip: để căn chỉnh lại đầu dòng, chọn đoạn cần căn chỉnh hoặc CTRL-A để chọn toàn file, rồi nhấn CTRL-I). Khi dòng quá dài, xuống dòng tại vị trí sau dấu phẩy `,` phẩy như thư mặc định của framework Cocoa.

```swift
// xuống dòng và thụt đầu dòng khi dòng dài
func myFunctionWithManyParameters(parameterOne: String,
                                  parameterTwo: String,
                                  parameterThree: String) {
    print("\(parameterOne) \(parameterTwo) \(parameterThree)")
}

// trường hợp không có dấu phẩy
if myFirstValue > (mySecondValue + myThirdValue)
    && myFourthValue == .someEnumValue {
    print("Hello, World!")
}
```

* **10** Khi gọi functions có nhiều thông số đầu vào, xuống dòng với mỗi thông số từ vị trí thứ hai.

```swift
someFunctionWithManyArguments(
    firstArgument: "Hello, I am a string",
    secondArgument: resultFromSomeFunction(),
    thirdArgument: someOtherLocalProperty)
```
Note: Khi một dòng dài thì cần xuống dòng theo mẫu trên. Với những dòng ngắn thì hạn chế xuống dòng gây rối code. Ví dụ:

```swift
// Nên
let firstCondition = x == firstReallyReallyLongPredicateFunction()
let secondCondition = y == secondReallyReallyLongPredicateFunction()
let thirdCondition = z == thirdReallyReallyLongPredicateFunction()
if firstCondition && secondCondition && thirdCondition {
    // do something
}

// Không nên
if x == firstReallyReallyLongPredicateFunction()
    && y == secondReallyReallyLongPredicateFunction()
    && z == thirdReallyReallyLongPredicateFunction() {
    // do something
}
```

## Cách đặt tên biến, hàm, class...

* **1** Swift không nên đặt tên gồm tiền tố như Objective-C (ví dụ: sử dụng `GuybrushThreepwood` thay vì `LIGuybrushThreepwood`).

* **2** Sử dụng `PascalCase` cho tên kiểu dữ liệu (ví dụ: `struct`, `enum`, `class`, `typedef`, `associatedtype`...).

* **3** Sử dụng `camelCase` (khởi đầu bằng chữ cái thường) cho tên function, method, property, constant, variable, argument names, enum cases...

* **4** Với trường hợp tên là tổng hợp của các chữ viết tắt (chúng ta cần viết hoa cả tên) mà lại đứng ở đầu tên hàm, biến... Chúng ta nên viết thường cả từ đó.

```swift
// "HTML" là khởi đầu của tên hằng, nên chúng ta sử dụng viết thường "html"
let htmlBodyContent: String = "<p>Hello, World!</p>"
// nên sử dụng ID thay vì Id
let profileID: Int = 1
// nên sử dụng URLFinder thay vì UrlFinder
class URLFinder {
    /* ... */
}
```

* **5** Tên phải mang ý nghĩa mô tả chức năng, tính chất một cách rõ ràng. Không đặt tên chung chung, mơ hồ.

```swift
// nên
class RoundAnimatingButton: UIButton { /* ... */ }

// không nên
class CustomButton: UIButton { /* ... */ }
```

* **6** Tên không nên được viết tắt, viết gọn quá nhiều gây khó hiểu.

```swift
// nên
class RoundAnimatingButton: UIButton {
    let animationDuration: NSTimeInterval

    func startAnimating() {
        let firstSubview = subviews.first
    }

}

// không nên
class RoundAnimating: UIButton {
    let aniDur: NSTimeInterval

    func srtAnmating() {
        let v = subviews.first
    }
}
```

* **10** Khi đặt tên param cho func, nên đặt tên rõ ràng, dễ hiểu, làm sao đọc tên func và param như đọc một câu văn có nghĩa.

* **11** Theo hướng dẫn đặt tên [Apple's API Design Guidelines](https://swift.org/documentation/api-design-guidelines/), Tên của `protocol` nên được đặt bằng danh từ mô tả những gì nó có thể làm (ví dụ: `Collection`) và thêm đuôi `able`, `ible`, hoặc `ing` nếu `protocol` có khả năng làm những việc đó (ví dụ: `Equatable`, `ProgressReporting`).

```swift
// tên protocol miêu tả chức năng của protocol
protocol TableViewSectionProvider {
    func rowHeight(at row: Int) -> CGFloat
    var numberOfRows: Int { get }
    /* ... */
}

// protocol có khả năng làm một việc
protocol Loggable {
    func logCurrentState()
    /* ... */
}

// mở rộng chức năng
protocol InputTextViewProtocol {
    func sendTrackingEvent()
    func inputText() -> String
    /* ... */
}
```

Tên các func, closure... dùng động từ, còn lại dùng danh từ.

## Coding Style

### General

* **1.** Sử dụng `let` thay vì `var` bất cứ khi nào có thể.

* **2.** Ưu tiên sử dụng các phương thức mới: `map`, `filter`, `reduce`... thay vì cấu trúc logic phức tạp.

```swift
// nên
let stringOfInts = [1, 2, 3].flatMap { String($0) }
// ["1", "2", "3"]

// không nên
var stringOfInts: [String] = []
for integer in [1, 2, 3] {
    stringOfInts.append(String(integer))
}

// nên
let evenNumbers = [4, 8, 15, 16, 23, 42].filter { $0 % 2 == 0 }
// [4, 8, 16, 42]

// không nên
var evenNumbers: [Int] = []
for integer in [4, 8, 15, 16, 23, 42] {
    if integer % 2 == 0 {
        evenNumbers.append(integer)
    }
}
```

* **3.** Chú ý khi sử dụng protocol/delegate để tránh retain cycle count, các thể hiện của protocol nên để `weak`.

* **4.** Chú ý khi sử dụng `self` trong closure để tránh retain cycle. Tham khảo [capture list](https://developer.apple.com/library/ios/documentation/swift/conceptual/Swift_Programming_Language/Closures.html#//apple_ref/doc/uid/TP40014097-CH11-XID_163)

```swift
// sử dụng weak, unowned...
myFunctionWithEscapingClosure() { [weak self] (error) -> Void in 
    // với weak, có thể dùng optional

    self?.doSomething()

    // hoặc unwrap optional

    guard let strongSelf = self else {
        return
    }

    strongSelf.doSomething()
}
```

* **5.** Không dùng dấu ngoặc đơn bừa bãi.

```swift
// nên
if x == y {
    /* ... */
}

// không nên
if (x == y) {
    /* ... */
}
```

* **6.** Không dùng `self.` trừ các trường hợp bắt buộc.

* **7.** Với class func và property (gọi func hoặc property mà không cần tạo instance). Dùng `static` cho func hoặc property khi không có nhu cầu override, ngược lại dùng `class` Only use `class`. Nếu dùng được protocol thì ưu tiên dùng protocol hơn.

### Custom Operators

Hạn chế tối đa override các thuộc tính, func global. 
Có thể tạo các func để làm việc đó. Hoặc tạo các custom component bằng cách subclass.

### Optionals

Hiểu đúng về optionals, không force unwrap optionals bừa bãi dẫn tới mất kiểm soát.

* **1.** Trường hợp duy nhất force unwrap là với `@IBOutlet`. Các trường hợp còn lại, nếu thuộc tính luôn phải có giá trị thì không dùng optional, nếu thuộc tính có thể nil thì dùng optional một cách bình thường, không force unwrap linh tinh.

* **2.** Không sử dụng `as!` hay `try!`.

* **3.** Nếu không có nhu cầu sử dụng giá trị trong optional, không nên lấy giá trị ra.

```swift
// nên
if someOptional != nil {
    // do something
}

// không nên
if let _ = someOptional {
    // do something
}
```

* **4.** Sử dụng tên giống nhau cho biến, hằng khi unwrap.

```swift
guard let myValue = myValue else {
    return
}
```

### Properties

* **1.** Với thuộc tính read-only, không cần đặt `get {}`.

```swift
var computedProperty: String {
    if someBool {
        return "I'm a mighty pirate!"
    }
    return "I'm selling these fine leather jackets."
}
```

* **2.** Nên sử dụng các tên mặc định `newValue`/`oldValue` cho `willSet`/`didSet` và `set` thay vì đặt một tên mới.

```swift
var storedProperty: String = "I'm selling these fine leather jackets." {
    willSet {
        print("will set to \(newValue)")
    }
    didSet {
        print("did set from \(oldValue) to \(storedProperty)")
    }
}

var computedProperty: String  {
    get {
        if someBool {
            return "I'm a mighty pirate!"
        }
        return storedProperty
    }
    set {
        storedProperty = newValue
    }
}
```

* **3.** Có thể định nghĩa singleton theo cách sau

```swift
class PirateManager {
    static let shared = PirateManager()

    /* ... */
}
```

### Closures

Chú ý về @escaping closure và @nonEscaping closure

* Cần viết tên closure nếu ý nghĩa của closure chưa rõ

```swift
// không cần đặt tên closure
doSomething(1.0) { (parameter1) in
    print("Parameter 1 is \(parameter1)")
}

// no trailing closure
doSomething(1.0, success: { (parameter1) in
    print("Success with \(parameter1)")
}, failure: { (parameter1) in
    print("Failure with \(parameter1)")
})
```

### Structure

`MARK:` là câu lệnh comment thích hợp để chia section, group các thành phần vào với nhau. Thuận lợi cho việc tìm kiếm.

```swift

import SomeExternalFramework

class FooViewController : UIViewController, FoobarDelegate {

    let foo: Foo

    private let fooStringConstant = "FooConstant"
    private let floatConstant = 1234.5

    // MARK: Lifecycle

    // Custom initializers go here

    // MARK: View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // ...
    }

    // MARK: Layout

    private func makeViewConstraints() {
        // ...
    }

    // MARK: User Interaction

    func foobarButtonTapped() {
        // ...
    }

    // MARK: FoobarDelegate

    func foobar(foobar: Foobar, didSomethingWithFoo foo: Foo) {
        // ...
    }

    // MARK: Additional Helpers

    private func displayNameForFoo(foo: Foo) {
        // ...
    }

}
```
## Documentation/Comments

### Documentation

Theo nguyên tắc S trong [SOLID](https://viblo.asia/p/solid-la-gi-ap-dung-cac-nguyen-ly-solid-trong-thiet-ke-maGK7WELKj2): Mỗi một hàm, class chỉ giữ một chức năng duy nhất.
Tuy nhiên có những hàm, class phức tạp, nhiều tính năng ta cần chú thích.
Chú thích trong ngôn ngữ Swift được Apple giới thiệu tại [Swift's comment markup Apple's Documentation](https://developer.apple.com/library/tvos/documentation/Xcode/Reference/xcode_markup_formatting_ref/Attention.html#//apple_ref/doc/uid/TP40016497-CH29-SW1).

Một số điểm chính:

* **1.** Tối đa 160 kí tự một dòng.

* **2.** Cho dù nội dung comment chỉ trong một dòng, vẫn nên dùng (`/** */`).

* **3.** Sử dụng cú pháp `- parameter` mới thay cho cú pháp cũ `:param:`.

```swift
class Human {
    /**
     This method feeds a certain food to a person.

     - parameter food: The food you want to be eaten.
     - parameter person: The person who should eat the food.
     - returns: True if the food was eaten by the person; false otherwise.
    */
    func feed(_ food: Food, to person: Human) -> Bool {
        // ...
    }
}
```

* **5.** Với những class phức tạp, chú thích một cách khoa học.

```swift
/**
 ## Feature Support

 This class does some awesome things. It supports:

 - Feature 1
 - Feature 2
 - Feature 3

 ## Examples

 Here is an example use case indented by four spaces because that indicates a
 code block:

     let myAwesomeThing = MyAwesomeClass()
     myAwesomeThing.makeMoney()

 ## Warnings

 There are some things you should be careful of:

 1. Thing one
 2. Thing two
 3. Thing three
 */
class MyAwesomeClass {
    /* ... */
}
```

* **6.** Viết chú thích ngắn gọn, súc tích nhất có thể.

### Một số chú ý về cách chú thích

* **1.** Luôn để `space` sau `//`.
* **2.** Luôn để comment ở riêng dòng, không lẫn với code.
* **3.** Khi sử dụng `// MARK: - whatever`, để cách dòng sau MARK.

```swift
class Pirate {

    // MARK: - instance properties

    private let pirateName: String

    // MARK: - initialization

    init() {
        /* ... */
    }

}
```

### Data Storage

Tuỳ vào dự án và các loại dữ liệu, chúng ta có thể lưu vào UserDefault, CoreData hoặc các thư viện thứ ba: Realm, SqlLite...
Tuy nhiên với các dữ liệu nhạy cảm như username, password, token... chúng ta cần mã hoá trước khi lưu. Phương án thuận tiện với lượng dữ liệu nhỏ cần lưu là dùng Keychain.
Thư viện hỗ trợ: [SSKeychain](https://github.com/soffes/sskeychain) hoặc [UICKeyChainStore](https://github.com/kishikawakatsumi/UICKeyChainStore).

## Diagnostics

### Crash Logs
 
 Sử dụng [Fabric][Fabric] để quản lý crash.
* [Fabric](https://get.fabric.io)
