[![N|Solid](http://ppclink.com/apps/wp-content/uploads/2016/06/logo_ppclink-2.png)](http://ppclink.com)

# Android Development Guidelines

## IDE

Use Android Studio.

## Support Android Versions

Default support version is Version 4.0 and later.

## SDK

Use latest SDK.

## Architecture

Use MVC Pattern

## Third party Library

Use gradle to install. And never use non-version controlled libraries.


## Default libraries
* Fabric
* Firebase analytics
* Butterknife
* Glide


# Quy tắc viết code trong Android
## 1. Java language rules

### 1.1. Đặt tên package
> Nhóm các file java theo từng loại. Cụ thể là activity riêng, model riêng, service riêng, Với cách này mỗi khi bạn cần một activity thì cứ đơn giản là vào package activities là được.
```sh
com.client.app - chỉ có class Application
com.client.app.views - bao gồm các activities
com.client.app.fragments - bao gồm các fragments
com.client.app.webservice - bao gồm các web services
com.client.app.adapters - bao gồm các adapters
com.client.app.db - bao gồm các classes liên quan đến db
com.client.app.models - bao gồm các models
com.client.app.utils - bao gồm các class utility
com.client.app.widgets - bao gồm các extended/custom views
com.client.app.services - bao gồm các services
com.client.app.animation - bao gồm các animations
```
> Nhóm các file theo từng chức năng. Ví dụ chức năng login:
```sh
com.client.app.login.views - bao gồm activity login
com.client.app.login.fragments - bao gồm các fragments của màn hình login
com.client.app.login.adapters - bao gồm các adapters của màn hình login
com.client.app.login.animation - bao gồm các animations của màn hình login
com.client.app.login.widgets - bao gồm các extended/custom views của màn hình login
```

### 1.2. Đừng bỏ qua trường hợp ngoại lệ

Bạn không bao giờ được làm như sau:

```java
void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) { }
}
```

Theo bạn nghĩ nó có thể không bao giờ xảy ra, nhưng một ngày nào đó bạn của bạn sẽ gặp vấn đề này. Bạn phải buộc xử lý nó theo một số cách.

* Ném ngoại lệ lên cho người gọi phương thức

```java
void setServerPort(String value) throws NumberFormatException {
    serverPort = Integer.parseInt(value);
}
```
* Ném lại ngoại lệ với lớp trừu tượng của bạn

```java
void setServerPort(String value) throws ConfigurationException {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) {
        throw new ConfigurationException("Port " + value + " is not valid.");
    }
}
```
* Thay thế một giá trị thích hợp, như giá trị mặc định chẳng hạn

```java
void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) {
        serverPort = 80;  // default port for server
    }
}
```

* Ném ngoại lệ vào một RuntimeException mới. Nhưng bạn chắc chắn rằng bạn muốn làm điều này, vì nó sẽ gây lỗi ứng dụng

```java
void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) {
        throw new RuntimeException("port " + value " is invalid, ", e);
    }
}
```

* Cuối cùng bạn sẽ loại bỏ nó nhưng phải có một lý do chính đáng

```java
void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) {
        // Method is documented to just ignore invalid user input.
        // serverPort will just be unchanged.
    }
}
```

### 1.3. Không được bắt ngoại lệ chung

```java
try {
    someComplicatedIOFunction();        // may throw IOException
    someComplicatedParsingFunction();   // may throw ParsingException
    someComplicatedSecurityFunction();  // may throw SecurityException
    // phew, made it all the way
} catch (Exception e) {                 // I'll just catch all exceptions
    handleError();                      // with one generic handler!
}
```
### 1.4. Import package khác vào trong class
Không nên: `import foo.*;`
Nên: `import foo.Bar;`

### 1.5. Đặt tên biến, phương thức và lớp.
Đặt tên biến, phương thức và lớp. Viết theo quy ước code Java, tên phải có nghĩa và viết hoa chữ cái đầu của từ.

| Good           | Bad            |
| -------------- | -------------- |
| `XmlHttpRequest` | `XMLHTTPRequest` |
| `getCustomerId`  | `getCustomerID`  |
| `String url`     | `String URL`     |
| `long id`        | `long ID`        |

## 2. Android language rules
### 2.1. Tên file trong thư mục resources
Viết thường tất cả các ký tự
#### 2.1.1. Tên file drawable
- Quy ước tên file drawable

| Asset Type   | Prefix            |		Example               |
|--------------| ------------------|-----------------------------|
| Action bar   | `ab_`             | `ab_stacked.9.png`          |
| Button       | `btn_`	            | `btn_send_pressed.9.png`    |
| Dialog       | `dialog_`         | `dialog_top.9.png`          |
| Divider      | `divider_`        | `divider_horizontal.9.png`  |
| Icon         | `ic_`	            | `ic_star.png`               |
| Menu         | `menu_	`           | `menu_submenu_bg.9.png`     |
| Notification | `notification_`	| `notification_bg.9.png`     |
| Tabs         | `tab_`            | `tab_pressed.9.png`         |

- Quy ước tên icon

| Asset Type                      | Prefix             | Example                      |
| --------------------------------| ----------------   | ---------------------------- |
| Icons                           | `ic_`              | `ic_star.png`                |
| Launcher icons                  | `ic_launcher`      | `ic_launcher_calendar.png`   |
| Menu icons and Action Bar icons | `ic_menu`          | `ic_menu_archive.png`        |
| Status bar icons                | `ic_stat_notify`   | `ic_stat_notify_msg.png`     |
| Tab icons                       | `ic_tab`           | `ic_tab_recent.png`          |
| Dialog icons                    | `ic_dialog`        | `ic_dialog_info.png`         |

- Quy ước tên cho các trạng thái của 1 view object.

| State	       | Suffix          | Example                     |
|--------------|-----------------|-----------------------------|
| Normal       | `_normal`       | `btn_order_normal.9.png`    |
| Pressed      | `_pressed`      | `btn_order_pressed.9.png`   |
| Focused      | `_focused`      | `btn_order_focused.9.png`   |
| Disabled     | `_disabled`     | `btn_order_disabled.9.png`  |
| Selected     | `_selected`     | `btn_order_selected.9.png`  |

#### 2.1.2. Tên file layout

Tên file tương ứng với các thành phần giao diện màn hình, tính năng.

| Component        | Class Name             | Layout Name                   |
| ---------------- | ---------------------- | ----------------------------- |
| Activity         | `UserProfileActivity`  | `activity_user_profile.xml`   |
| Fragment         | `SignUpFragment`       | `fragment_sign_up.xml`        |
| Dialog           | `ChangePasswordDialog` | `dialog_change_password.xml`  |
| AdapterView item | ---                    | `item_person.xml`             |
| Partial layout   | ---                    | `partial_stats_bar.xml`       |


#### 2.1.3. Tên file menu

Viết tương ứng với tên màn hình chứa menu đó.
VD: menu_activity_home.xml

#### 2.1.4. Values files

- Đặt tên theo chuẩn của Android: strings.xml, styles.xml, colors.xml, dimens.xml, attrs.xml
- Các giá trị trong thư mục này do bên kịch bản và đồ họa cung cấp.
- Tất cả text hiển thị trên app đều lấy từ strings.xml, giá trị màu lấy từ colors.xml, các giá trị kích thước, size chữ lấy từ dimens.xml
- Không được phép fix cứng giá trị trong code.

### 2.2. Quy tắc sử dụng file ảnh để support resize nhiều màn hình
- Tất cả các icon trong app có đồ họa đơn giản nên để dạng vector lưu dưới dạng file xml
- Còn các ảnh sử dụng trong app thì tạo ra các file tương ứng với tỉ lệ pixel do android quy ước trong các thư mục drawable-xxxhdpi, drawable-xxhdpi, drawable-xhdpi, drawable-hdpi, drawable-mdpi,...

### 2.3. Add thư viện trong gradle
Gom nhóm thư viện và comment cho dễ hiểu:
VD:
```sh
//Firebase Authentication
 implementation 'com.google.firebase:firebase-core:16.0.6'
 implementation 'com.google.firebase:firebase-auth:16.1.0'

//fabric
  compile('com.crashlytics.sdk.android:crashlytics:2.9.8@aar') {
    transitive = true;
  }
```
### 2.4. Comment
Tất cả các hàm đều được comment chú thích theo chuẩn java duới dạng
```sh
    /**
     * sum 2 number
     * @param a
     * @param b
     * @return int
     */
    public int sumNumber(int a, int b) {
        return a + b;
    }
```

### 2.5. Cấu trúc project Android

![Alt text](https://bitbucket.org/ppclink/ppclink-development-rules/raw/29fd4a35f738dea602052b6321bccddc259cd911/project_structure.png)
